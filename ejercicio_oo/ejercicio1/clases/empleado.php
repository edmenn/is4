<?php
require_once("vendor/autoload.php");
 class Empleado extends Persona{
    protected $CodigoEmpleado;
    protected $NombreEmpresa;
    protected $DireccionEmpresa;

    function __construct($nombre, $apellido, $cedula, $direccion, $edad, $Telefonos){
        parent::__construct($nombre, $apellido, $cedula, $direccion, $edad,$Telefonos);

    }
    public function setCodigoEmpleado($CodigoEmpleado){
        $this->CodigoEmpleado=$CodigoEmpleado;
    }
    public function setNombreEmpresa($NombreEmpresa){
        $this->NombreEmpresa=$NombreEmpresa;
    }
    public function setDireccionEmpresa($DireccionEmpresa){
        $this->DireccionEmpresa=$DireccionEmpresa;
    }
    public function getCodigoEmpleado(){
        return $this->CodigoEmpleado;
    }
    public function getNombreEmpresa(){
        return $this->NombreEmpresa;
    }
    public function getDireccionEmpresa(){
        return $this->DireccionEmpresa;
    }
 }

 



?>