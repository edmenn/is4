import AppListing from '../app-components/Listing/AppListing';

Vue.component('agenda-listing', {
    mixins: [AppListing]
});