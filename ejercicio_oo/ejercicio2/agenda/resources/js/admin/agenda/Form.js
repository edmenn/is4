import AppForm from '../app-components/Form/AppForm';

Vue.component('agenda-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                Nombre:  '' ,
                Apellido:  '' ,
                Descripción:  '' ,
                Teléfono:  '' ,
                
            }
        }
    }

});