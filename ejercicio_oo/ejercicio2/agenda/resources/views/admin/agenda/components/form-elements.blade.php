<div class="form-group row align-items-center" :class="{'has-danger': errors.has('Nombre'), 'has-success': fields.Nombre && fields.Nombre.valid }">
    <label for="Nombre" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.agenda.columns.Nombre') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.Nombre" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('Nombre'), 'form-control-success': fields.Nombre && fields.Nombre.valid}" id="Nombre" name="Nombre" placeholder="{{ trans('admin.agenda.columns.Nombre') }}">
        <div v-if="errors.has('Nombre')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('Nombre') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('Apellido'), 'has-success': fields.Apellido && fields.Apellido.valid }">
    <label for="Apellido" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.agenda.columns.Apellido') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.Apellido" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('Apellido'), 'form-control-success': fields.Apellido && fields.Apellido.valid}" id="Apellido" name="Apellido" placeholder="{{ trans('admin.agenda.columns.Apellido') }}">
        <div v-if="errors.has('Apellido')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('Apellido') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('Descripción'), 'has-success': fields.Descripción && fields.Descripción.valid }">
    <label for="Descripción" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.agenda.columns.Descripción') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.Descripción" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('Descripción'), 'form-control-success': fields.Descripción && fields.Descripción.valid}" id="Descripción" name="Descripción" placeholder="{{ trans('admin.agenda.columns.Descripción') }}">
        <div v-if="errors.has('Descripción')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('Descripción') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('Teléfono'), 'has-success': fields.Teléfono && fields.Teléfono.valid }">
    <label for="Teléfono" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.agenda.columns.Teléfono') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.Teléfono" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('Teléfono'), 'form-control-success': fields.Teléfono && fields.Teléfono.valid}" id="Teléfono" name="Teléfono" placeholder="{{ trans('admin.agenda.columns.Teléfono') }}">
        <div v-if="errors.has('Teléfono')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('Teléfono') }}</div>
    </div>
</div>


