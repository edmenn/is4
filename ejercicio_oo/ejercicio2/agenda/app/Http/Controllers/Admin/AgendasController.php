<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Agenda\BulkDestroyAgenda;
use App\Http\Requests\Admin\Agenda\DestroyAgenda;
use App\Http\Requests\Admin\Agenda\IndexAgenda;
use App\Http\Requests\Admin\Agenda\StoreAgenda;
use App\Http\Requests\Admin\Agenda\UpdateAgenda;
use App\Models\Agenda;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class AgendasController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexAgenda $request
     * @return array|Factory|View
     */
    public function index(IndexAgenda $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Agenda::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'Nombre', 'Apellido', 'Descripción', 'Teléfono'],

            // set columns to searchIn
            ['id', 'Nombre', 'Apellido', 'Descripción']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.agenda.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.agenda.create');

        return view('admin.agenda.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAgenda $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreAgenda $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the Agenda
        $agenda = Agenda::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/agendas'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/agendas');
    }

    /**
     * Display the specified resource.
     *
     * @param Agenda $agenda
     * @throws AuthorizationException
     * @return void
     */
    public function show(Agenda $agenda)
    {
        $this->authorize('admin.agenda.show', $agenda);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Agenda $agenda
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Agenda $agenda)
    {
        $this->authorize('admin.agenda.edit', $agenda);


        return view('admin.agenda.edit', [
            'agenda' => $agenda,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAgenda $request
     * @param Agenda $agenda
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateAgenda $request, Agenda $agenda)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Agenda
        $agenda->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/agendas'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/agendas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyAgenda $request
     * @param Agenda $agenda
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyAgenda $request, Agenda $agenda)
    {
        $agenda->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyAgenda $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyAgenda $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Agenda::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
