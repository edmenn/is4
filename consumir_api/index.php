
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 6 - Ejercitario 3</title>
  <link href="css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
    <?php
 
 $url ="https://dolar.melizeche.com/api/1.0/"; 
 
 $ch = \curl_init($url);

 // Configuring curl options 
 $options = array( 
    CURLOPT_RETURNTRANSFER => true,     
    CURLOPT_HTTPHEADER => array('Accept: application/json'), 
    CURLOPT_SSL_VERIFYPEER => false,     
 ); 
 // Setting curl options 
 \curl_setopt_array( $ch, $options );
 
 // Getting results 
 $response = curl_exec($ch); // Getting jSON result string   
 
 // Cerrar el recurso cURL y liberar recursos del sistema 
 \curl_close($ch);
 //print_r($response);
 
 $data = json_decode($response, true);     
 //var_dump($data);
$fecha=$data['updated'];
$date = strtotime($fecha);

$AmambayCompra=$data['dolarpy']['amambay']['compra'];
$AmambayVenta=$data['dolarpy']['amambay']['venta'];
$BcpCompra=$data['dolarpy']['bcp']['compra'];
$BcpVenta=$data['dolarpy']['bcp']['venta'];
$MydcambiosCompra=$data['dolarpy']['mydcambios']['compra'];
$MydcambiosVenta=$data['dolarpy']['mydcambios']['venta'];

echo "<br>";
echo "Fecha de Cotización: ".date('d/m/Y', $date)."<br>";
echo "Hora de Actualización: ".date('H:i:s', $date);


?>
    </div>
    <div class="desarrollo">
        <h1>MyD Cambios</h1>
        <h2>Cotización del Dolar</h2>
        <?php
        echo "<br>";
        echo "<h2>"."Compra: ".number_format($MydcambiosCompra,$decimals = 0, $decimal_separator = ',', $thousands_separator = '.')."</h2>";
        echo "<h2>"."Venta: ".number_format($MydcambiosVenta,$decimals = 0, $decimal_separator = ',', $thousands_separator = '.')."</h2>";
        ?>
        <form action="index.php" method="post">
        <p>
            <label for="numero_1">Ingrese el Monto</label>     
            <input type="number" name="monto" id="numero_1">
        </p>
        <select name="operacion" id="operacion">
            <option value="guaranies">Dolares a Guaranies</option>
            <option value="dolares">Guaranies a Dolares</option>
        </select>
        <input type="submit" value="Calcular" onclick="calcular()">
    </form>
    </form>
    <?php
        calcular($MydcambiosCompra,$MydcambiosVenta);
    ?>
  </div>
  <?php
            function calcular($MydcambiosCompra,$MydcambiosVenta) {
                error_reporting(E_ALL ^ E_NOTICE);
                        $monto = $_POST['monto'];
                $operacion = $_POST['operacion'];
                switch ($operacion) {
                    case 'guaranies':
                        $resultado = number_format($monto*$MydcambiosCompra,$decimals=0,$decimal_separator=',',$thousands_separator='.');
                        break;
                    case 'dolares':
                        $resultado = number_format($monto/$MydcambiosVenta,$decimals=2,$decimal_separator=',',$thousands_separator='.');
                    
                        break;
                    default:
                        break;
                }
                $tipo_resultado = gettype($resultado);

                    echo '<h4>El resultado es: <b>' . $resultado . '</b></h4>';
                
            }
        ?>
</body>
</html>
