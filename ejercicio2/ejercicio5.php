<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ejercicio 5</title>
<style>
		table {
		  margin: 0 auto;
		}
		table, th, td {
		  border: 1px solid black;
		  border-collapse: collapse;
		}
		td {
			text-align: center;
		}
		.tabla tr:nth-child(even) {
			background-color: Grey;
		}
	</style>
</head>
<body>
    <table class="tabla">
			<tr>
				<th>Números</th>
				<th>Resultados</th>
			</tr>
			<?php
				for ($i = 0; $i<= 10; $i++) {
					echo "<tr>";
					echo "<td>".$i." * 9 </td>";
					echo "<td>".$i * 9 ."</td>";
					echo "</tr>";
				}
			?>
	</table>
</body>
</html>