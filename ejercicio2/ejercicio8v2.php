<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ejercicio 8 V2</title>
<style>

th{
	border:silver 1px solid;
	background-color:yellow;	
}

table{
	border: silver 1px solid;
	border-collapse: collapse; 
	margin: 0 auto;
}

td{
	border:silver 1px solid;
	text-align: center;
}

tr:nth-child(odd) {
    background-color:#f2f2f2;
}

tr:nth-child(even) {
    background-color:#fbfbfb;
}
	</style>
</head>
<body>
<?php
    $numeros_pares=array();
    for ($i=1;$i<=900;$i++){
        $numeros_pares[].=number_format(rand(1,5000)*2,0,",",".");
        //$numeros_pares[].=$i;
    }
    $tabla="";
    $i=0;
    $k=0;
    $j=0;
    $tabla.=<<<HTML
    <table>
HTML;
    for($i=1;$i<=30;$i++){
        $tabla.=<<<HTML
            <tr>
HTML;
        for($k=0;$k<=29;$k++){
            $tabla.=<<<HTML
                <td>$numeros_pares[$j]</td>
HTML;
            $j++;
        }
        $tabla.=<<<HTML
            </tr>
HTML;
    }
    $tabla.=<<<HTML
    </table>
HTML;
echo $tabla;
?>
</body>
</html>