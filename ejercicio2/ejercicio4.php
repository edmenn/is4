<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ejercicio 4</title>
</head>
<body>
 <?php
 //CODIGO PHP
 $variable=24.5;
 echo "<b>La variable $variable :</b> ";
 echo "<b>Es de un tipo de dato:</b> ".gettype($variable)."<br><br>";
 
 $variable="HOLA";
 echo "<b>La variable $variable :</b> ";
 echo "<b>Es de un tipo de dato:</b> ".gettype($variable)."<br><br>";
 
 settype($variable, "integer");
 echo "<b>el resultado del var_dump es</b> ";
 var_dump($variable);
 ?>
</body>
</html>