<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 10 - Ejercitario 3</title>
  <link href="css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Hacer un script PHP que declare un vector de 50 elementos con valores aleatorios enteros entre 1
            y 1000. El script debe determinar cuál es el elemento con mayor valor en el vector, se debe
            imprimir un mensaje con el valor y el índice en donde se encuentra.
            Mensaje de ejemplo: El elemento con índice 8 posee el mayor valor que 789.</h2>
        <p><b>Observación:</b>El alumno deberá crear sus propias funciones para realizar este ejercicio.</p>
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <?php
        $usados = []; //declaramos el array

        //cargamos el array con los numeros al azar

        $max = 1000;
        $aleatorio = mt_rand(1, $max); //Genereamos aleatorio
        $usados[] = $aleatorio; //Guardamos el primero en un array ya que el primero no puede estar repetido

        for ($i = 0; $i < 49; $i++) {
    
         $aleatorio = mt_rand(1, $max); //Generamos aleatorio
            while(in_array($aleatorio,$usados)) { //buscamos que no este repetido
             $aleatorio = mt_rand(1, $max);
            }

            $usados[] = $aleatorio;    //No esta repetido, luego guardamos el aleatorio
        }
        $num_max=encontrarMaximo($usados);
        $indice=array_search($num_max, $usados); 
        //imprimimos el resultado de la suma de los numeros dentro del array
        echo "<h4>El elemento con índice $indice posee el mayor valor que es $num_max</h4>";
        
        echo '<table>
        <tr>
        <th>Ìndice</th>
        <th>Número</th>
        </tr>';  
        for ($i=0; $i <= sizeof($usados)-1; $i++) { 
            
                echo '<tr>
                    <td>'.$i.'</td>
                    <td>'.$usados[$i].'</td>
                    </tr>';
            
        }
        echo '</table>';
       $indice=0;
        function encontrarMaximo($usados){
            $valor=$usados[0];
            for ($i=1; $i <= 49; $i++){
                if ($valor<$usados[$i]){
                    $valor=$usados[$i];
                }
            }
            return $valor;
        };
        
    ?>
  </div>
</body>
</html>