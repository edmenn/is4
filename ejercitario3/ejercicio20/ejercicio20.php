<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 20 - Ejercitario 3</title>
  <link href="../css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Leer un archivo agenda.txt (creado por el alumno), este archivo contendrá el nombre y el apellido
            de personas.
            Se debe crear una función PHP que busque un nombre y un apellido en dicho archivo e imprima un
            mensaje si se encontró o no se encontró el nombre y apellido en el archivo. Nombre y apellido son
            variables que se debe introducir el usuario por formulario.</h2>
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <form role="form" method="POST" action="buscar.php">
            <div>
                <div></div>
                    <div>
                        <h2>Agenda</h2>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div></div>
                    <div>
                        <div>
                            <label>Nombre</label>
                            <div>
                                <input type="text" name="nombre" id="nombre" placeholder="Tu Nombre" required>
                            </div>
                            <div>
                                <label>Apellido</label>
                            </div>   
                            <div>
                                <input type="text" name="apellido" id="apellido" placeholder="Tu apellido" required>
                            </div>
                        </div>
                        <div>
                            <button type="submit"><i></i>Buscar</button>
                        </div>
        </form>

  </div>
</body>
</html>