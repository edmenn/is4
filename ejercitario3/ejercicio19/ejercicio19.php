<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 18 - Ejercitario 3</title>
  <link href="../css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Dado un archivo con la siguiente información: una lista de matrículas, nombre, apellido y 3 notas
            parciales para un grupo alumnos (el archivo puede tener un número variable de líneas de texto).
            Procesar estos datos y construir un nuevo archivo que contenga solamente la matrícula y la
            sumatoria de notas de los alumnos.</h2>
        <img src="../images/ej19.JPG" alt="">
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <?php
        $pathLectura   = "notas.txt";
        $pathEscritura = "sumaNotas.txt";
        cargarNotas($pathLectura,$pathEscritura);


        function cargarNotas($pathLectura,$pathEscritura)
        {
            $Lectura   = fopen($pathLectura,"r+");
            $Escritura = fopen($pathEscritura,"r+");
            $calificaciones = file($pathLectura);
            
            foreach ( $calificaciones as $value ) 
            {
                list($matricula,$nombre,$apellido,$nota1,$nota2,$nota3) = explode(" ", $value);
                $notaFinal = (integer)$nota1 + (integer)$nota2 + (integer)$nota3;
                $cadena    = $matricula ." ". $notaFinal."\n";
                fwrite($Escritura, $cadena,strlen($cadena));
            }
            
            fclose($Escritura);
            fclose($Lectura);
            echo "El archivo de suma de notas se generó con éxito.";
        }
        ?>

  </div>
</body>
</html>