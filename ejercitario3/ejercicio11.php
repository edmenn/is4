<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 11 - Ejercitario 3</title>
  <link href="css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Realizar un script PHP que haga lo siguiente:
            Declare un vector indexado cuyos valores sean urls (esto simula un log de acceso de un usuario).
            El script debe contar e imprimir cuantas veces accedió el usuario al mismo url (no se deben repetir
            os url).</h2>
        <p><b>Observación:</b>El alumno deberá crear sus propias funciones para realizar este ejercicio.</p>
        <img src="images/ej11.JPG" alt="">
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <?php
        
     /*    $urls=array("http://www.abc.com.py","http://www.abc.com.py","https://mail.google.com","http://gitref.org","https://mail.google.com",
                    "http://aulavirtual.uc.edu.py/aulas/","http://www.hattrick.org","http://aulavirtual.uc.edu.py/aulas/","http://aulavirtual.uc.edu.py/aulas/",
                    "http://www.abc.com.py"); */
        
                    $urls=Array
                    (
                        Array (
                                "link" => "http://www.abc.com.py",
                                "accesos" => "1",
                            ),
                    
                        Array (
                                "link" => "http://www.abc.com.py",
                                "accesos" => "1",
                            ),
                        Array (
                                "link" => "https://mail.google.com",
                                "accesos" => "1",
                            ),
                        Array (
                                "link" => "http://gitref.org",
                                "accesos" => "1",
                            ),
                    
                        Array (
                                "link" => "https://mail.google.com",
                                "accesos" => "1",
                            ),
                        Array (
                                "link" => "http://aulavirtual.uc.edu.py/aulas/",
                                "accesos" => "1",
                            ),
                        Array (
                                "link" => "http://www.hattrick.org",
                                "accesos" => "1",
                            ),
                        Array (
                                "link" => "http://aulavirtual.uc.edu.py/aulas/",
                                "accesos" => "1",
                            ),
                    
                        Array (
                                "link" => "http://aulavirtual.uc.edu.py/aulas/",
                                "accesos" => "1",
                            ),
                        Array (
                                "link" => "http://www.abc.com.py",
                                "accesos" => "1",
                            ),
                    
                    );
                    
                    $nuevo_array = array();
                    foreach ($urls as $t) {
                        $repetir = false;
                        for($i=0; $i < count($nuevo_array); $i++) {
                            if ($nuevo_array[$i]['link']==$t['link']) {
                                $nuevo_array[$i]['accesos'] += $t['accesos'];
                                $repetir = true;
                                break;
                            }
                        }
                        if ($repetir == false)
                            $nuevo_array[] = array('link' => $t['link'], 'accesos' => $t['accesos']);
                    }
                    echo '<h4>Los accesos del usuario son los siguientes:</h4>';
                    echo '<table>';
                    echo '<th> Link';
                    echo '<th> Accesos';
                    foreach ( $nuevo_array as $r ) {
                        echo '<tr>';
                        foreach ( $r as $v ) {
                            echo '<td>'.$v.'</td>';
                    }
                        echo '</tr>';
                    }
                    echo '</table>';
                ?>
  </div>
</body>
</html>