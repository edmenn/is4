<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 16 - Ejercitario 3</title>
  <link href="css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Representar el siguiente árbol binario en PHP y recorrerlo en pre-orden.</h2>
        <img src="./images/ej16.JPG" alt="">
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <?php
		$arbolBinario = array(3,6,4,14,9,900,45,100,30,40,15,-1,-1,-1,-1);

		function recorridoPreOrden($arbolBinario,$i)
		{
			if( $i >= (count($arbolBinario)-1) )
				return;
			
			if( $arbolBinario[$i] != -1 )
				echo $arbolBinario[$i]." ";
			
			recorridoPreOrden($arbolBinario, 2*$i+1);
			recorridoPreOrden($arbolBinario, 2*$i+2);
		}

		function recorridoPostOrden($arbolBinario,$i)
		{
			if( $i >= (count($arbolBinario)-1) )
				return;

			recorridoPostOrden($arbolBinario, 2*$i+1);
			recorridoPostOrden($arbolBinario, 2*$i+2);
			
			if( $arbolBinario[$i] != -1 )
				echo $arbolBinario[$i]." ";
		}

		function recorridoInOrden($arbolBinario,$i)
		{
			if( $i >= (count($arbolBinario)-1) )
				return;

			recorridoInOrden($arbolBinario, 2*$i+1);
			if( $arbolBinario[$i] != -1 )
				echo $arbolBinario[$i]." ";
			recorridoInOrden($arbolBinario, 2*$i+2);
		}
		echo "Pre Orden: ";
		recorridoPreOrden($arbolBinario, 0);
	?>
  </div>
</body>
</html>