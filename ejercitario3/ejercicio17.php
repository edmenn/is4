<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 17 - Ejercitario 3</title>
  <link href="css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Representar una estructura de árbol n-ario utilizando arrays de PHP. Representar un árbol similar
            al de la figura.</h2>
        <img src="./images/ej17.JPG" alt="">
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <?php
function addToTree(&$arbol, $rama) {
    $len = count($rama);

    for($i=0; $i < $len; $i++ ) {

        if( !isset( $arbol[ $rama[$i] ] ) ) {
            $arbol[ $rama[$i] ]  = [];
        }

        $arbol = &$arbol[ $rama[$i] ];
    }   
}

function makeTree($values) 
{
    $tree = [];
    $rTree = &$tree;
    foreach( $values as $item ) {
        addToTree($rTree,$item);
    }   
    return $tree;
}

$numeric = array(
                0 => array("R"),//padre
                1 => array("R","S"),//padre R, Hijo S
                2 => array("R","T"),// padre R, Hijo T
                3 => array("R","D"),
                4 => array("R","S","U"),// padre R, hijo de R => S, Hijo de S => U
                5 => array("R","S","V"),
                6 => array("R","S","U","L"),// padre R, hijo de R => S, Hijo de S => U, Hijo de U => L
                7 => array("R","S","U","Q"),
                8 => array("R","S","U","W"),
                9 => array("R","T","G"),
                10 => array("R","T","F"),
                11 => array("R","T","G","C"),
                12 => array("R","T","G","K"),
                13 => array("R","D","H"),
                14 => array("R","D","J"),
                15 => array("R","D","H","A"),
                16 => array("R","D","H","X"),
                17 => array("R","D","J","Z"),
                18 => array("R","D","J","I")
);

$numericTree = makeTree($numeric);

echo "<pre class='arbol'>".json_encode($numericTree,JSON_PRETTY_PRINT) ."</pre>";
    ?>
  </div>
</body>
</html>