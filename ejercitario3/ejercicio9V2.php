<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 9 sin repetir - Ejercitario 3</title>
  <link href="css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Realizar un script en PHP que declare un vector de 100 elementos con valores aleatorios enteros
            entre 1 y 100 y sume todos los valores. El script debe imprimir un mensaje con la sumatoria de los
            elementos</h2>
        <p><b>Observación:</b>El alumno deberá crear sus propias funciones para realizar este ejercicio.</p>
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <?php
        $array = []; //declaramos el array

        //cargamos el array con los numeros al azar
/*         for ($i=0; $i < 100; $i++) { 
            $numAzar = mt_rand(1, 100);
            array_push($array, $numAzar);
        } */
        $max = 100;
        $aleatorio = mt_rand(1, $max); //Genereamos aleatorio
        $usados[] = $aleatorio; //Guardamos el primero en un array ya que el primero no puede estar repetido

        for ($i = 0; $i < $max-1; $i++) {
    
         $aleatorio = mt_rand(1, $max); //Generamos aleatorio
            while(in_array($aleatorio,$usados)) { //buscamos que no este repetido
             $aleatorio = mt_rand(1, $max);
            }

            $usados[] = $aleatorio;    //No esta repetido, luego guardamos el aleatorio
        }

        //damos formato a la suma
        $suma = number_format(array_sum($usados), 0, ',', '.');
        
        //imprimimos el resultado de la suma de los numeros dentro del array
        echo "<h4>La suma de los números sin repetir es: $suma</h4>";
        //imprimimos el array dandole formato
/*         echo '<pre>';
            print_r($array);
        echo '</pre>'; */
        $tabla="";
        $i=0;
        $k=0;
        $j=0;
        $tabla.=<<<HTML
        <table>
    HTML;
        for($i=1;$i<=10;$i++){
            $tabla.=<<<HTML
                <tr>
    HTML;
            for($k=0;$k<=9;$k++){
                $tabla.=<<<HTML
                    <td>$usados[$j]</td>
    HTML;
                $j++;
            }
            $tabla.=<<<HTML
                </tr>
    HTML;
        }
        $tabla.=<<<HTML
        </table>
    HTML;
    echo $tabla;
    ?>
  </div>
</body>
</html>