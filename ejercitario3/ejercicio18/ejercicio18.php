
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 18 - Ejercitario 3</title>
  <link href="../css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Realizar un contador de visitas en PHP utilizando un archivo para mantener la cantidad de visitas a
            la página.</h2>
        <p><b>Observación:</b>El alumno deberá crear sus propias funciones para realizar este ejercicio.</p>
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <body>
    <?php
       $nombre = 'visitas.txt';
        if (file_exists($nombre)) {
            if(filesize($nombre) > 0){
                $contador = file_get_contents($nombre) + 1;

            } else {
                $contador=1;
            }
        } else {
            $file = fopen("visitas.txt", "w");
            fwrite($file, "1" . PHP_EOL);
            $contador=1;
            fclose($file);
        }
        file_put_contents($nombre, $contador);
            
        echo "<b>Contador de visitas en PHP: </b><br>";
        echo $contador;
    ?>
  </div>
</body>
</html>