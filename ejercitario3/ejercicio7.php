<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 7 - Ejercitario 3</title>
  <link href="css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Hacer un script en PHP que determine si una cadena dada es un palíndromo</h2>
        <p>Un palíndromo (del griego palin dromein, volver a ir hacia atrás) es una palabra, número o frase
            que se lee igual hacia adelante que hacia atrás. Si se trata de un número, se llama capicúa.
            Habitualmente, las frases palindrómicas se resienten en su significado cuanto más largas son</p>
            <p>Ejemplos: radar, Neuquén, anilina, ananá, Malayalam</p>
        <p><b>Observación:</b>El alumno deberá crear sus propias funciones para realizar este ejercicio.</p>
    </div>
    <div class="desarrollo">
        <h1>Formulario</h1>
        <?php
        $str=<<<HTML
            <form action="#" method="post">
                <div>
                    <label for="palabra">Verificar si es Palindromo:</label>
                        <input type="text" name="palabra" placeholder="Introduzca la palabra" />
                </div>
                <br/>
                <div class="button">
                    <button type="submit">Verificar</button>
                </div>
            </form>
        HTML;

        if( !isset($_POST['palabra']))
        {
            echo $str;
        } else {
            echo $largo = "";
            $palabraOriginal = $_POST['palabra'];
            $palabra=strtoupper($palabraOriginal);  //convierto todo a MAYUSCULAS para que el resultado no varie si pongo una letra en Mayusculas.    
            $resultado = verificarPalindromo($palabra);
            echo "<p class= centrado>La palabra <b>$palabraOriginal</b>, <b>$resultado</b> es Palíndromo </p>";
            echo "<h4><a href='ejercicio7.php'>Volver al Verificador</a></h4>";
        }
        function verificarPalindromo($palabra)
        {
            $largo = strlen($palabra) - 1;
            if ($largo >= 2) 
            {
                for ($i = 0; $i <= $largo; $i++)
                {
                    if (!(substr($palabra, $i, 1) == substr($palabra, ($largo - $i), 1)))
                    {
                        return "NO";
                        break;
                    }
                }
                return "SI";
            } else {
                return "Debe escribir una palabra de por lo menos 2 letras para poder evaluar.";
            }
        }
    ?>
  </div>
</body>
</html>