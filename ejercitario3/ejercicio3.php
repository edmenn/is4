<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 3 - Ejercitario 3</title>
  <link href="css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <p>Generar un script PHP que:</p>
           <ul>
               <li>cree una tabla HTML con los números pares que existen entre 1 y N.</li>
               <li>El número N estará definido por una constante PHP</li>
               <li>El valor de la constante N debe ser un número definido por el alumno.</li>
           </ul>
           <p><b>Observación:</b>El script PHP debe estar embebido en una página HTML.</p>
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <h4>El número elegido es 100</h4>
        <?php
        const numero = 100;
      
            echo '<table>
                    <th>Números Pares</th>';   
        for ($i=2; $i <= numero; $i=$i+2) { 
            if ($i%2 == 0) {
                echo '<tr><td>'.$i.'</td>
                     </tr>';
            }
        }
        echo '</table>'
    ?>
  </div>
</body>
</html>