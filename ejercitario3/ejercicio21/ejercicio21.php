<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 21 - Ejercitario 3</title>
  <link href="../css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Hacer un formulario HTML que posee los siguientes tipos de elementos (el alumno debe crear un
            formulario que tenga sentido):
</h2>
        <ul>
            <li>Un campo de Texto</li>
            <li>Un campo Password</li>
            <li>Un select Simple</li>
            <li>Un select múltiple</li>
            <li>Un Checkbox</li>
            <li>Un Radio Button</li>
            <li>Un Text Area</li>
        </ul>
        <h2>Implementar un script PHP que capture los datos del anterior formulario e imprima en pantalla lo
            que introdujo el usuario de manera legible.</h2>
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <form action="imprimir.php" method="get">
        <div>
                            <label>Usuario</label>
                            <div>
                                <input type="text" name="usuario" id="usuario" placeholder="Tu Usuario" required>
                            </div>
                            <div>
                                <label>Password</label>
                            </div>   
                            <div>
                                <input type="password" name="pass" id="pass" placeholder="Password" required>
                            </div>
                            <div>
                                <label>Tipo de Cuenta</label>
                            </div>
                            <div>
                                <select name="tipoCuenta" id="tipoCuenta">
                                    <option value="Básico">Básico</option>
                                    <option value="Avanzado">Avanzado</option>
                                    <option value="Profesional">Profesional</option>
                                </select>
                            </div> 
                            <div>
                                <label>Intereses</label>
                            </div>
                            <div>
                                <select multiple name="selectMultiple[]">
                                    <option value="Internet">Internet</option>
                                    <option value="Tecnología">Tecnología</option>
                                    <option value="Programación">Programación</option>
                                    <option value="Deportes">Deportes</option>
                                    <option value="Cine">Cine</option>
                                </select>
                            </div>
                            <div>
                                <label>Conoce algun lenguaje de Programación?</label>
                            </div>

                            <div>
                            <input type="checkbox" name="lenguajes[]" value="Php">Php<br>
                            <input type="checkbox" name="lenguajes[]" value="Javascript">Javascript<br> 
                            <input type="checkbox" name="lenguajes[]" value="Python">Python<br>
                            <input type="checkbox" name="lenguajes[]" value="C#">C#<br>    
                        </div>
                            <div>
                                <label>Desea suscribirse al boletin de noticias por correo?</label>
                            </div>
                            <div>
                                <input type="radio" name="boletin" value="Sí">Si <br>
                                <input type="radio" name="boletin" value="No">No <br>
                            </div>

                        </div>

                        <div>
                                <label>En breves palabras, indique que le gustaria aprender.</label>
                        </div>
                        <div>
                        <textarea name="textArea" rows="10" cols="40"></textarea><br><br> 
                        </div>
                        <div>
                            <button type="submit"><i></i>Enviar</button>
                        </div>
        </form>
  </div>
</body>
</html>