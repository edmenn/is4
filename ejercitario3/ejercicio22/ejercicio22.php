<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 22 - Ejercitario 3</title>
  <link href="../css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Implementar un script PHP que implemente una funcionalidad básica de login:</h2>
        <ul>
            <li>El formulario debe tener dos campos: usuario, contraseña.</li>
            <li>El formulario deberá tener un botón de login.</li>
            <li>La verificación de usuario y contraseña se realizará a través de un archivo de datos (ya sea
                .txt, .dat).
            </li>
            <li>Si el usuario introducido existe en el archivo, se debe re-enviar a una página de
                bienvenida.
            </li>
            <li>Si el usuario introducido no existe en el archivo, se debe imprimir un mensaje de error de
                acceso.
            </li>
        </ul>
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <form role="form" method="POST" action="login.php">
            <div>
                <div></div>
                    <div>
                        <h2>Login</h2>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div></div>
                    <div>
                        <div>
                            <label>Usuario</label>
                            <div>
                                <input type="text" name="usuario" id="usuario" placeholder="Tu Usuario" required>
                            </div>
                            <div>
                                <label>Password</label>
                            </div>   
                            <div>
                                <input type="password" name="pass" id="pass" placeholder="Password" required>
                            </div>
                        </div>
                        <div>
                            <button type="submit"><i></i>Login</button>
                        </div>
        </form>

  </div>
</body>
</html>