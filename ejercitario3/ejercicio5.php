<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 5 - Ejercitario 3</title>
  <link href="css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<script>

</script>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Hacer un script PHP que genere un formulario HTML que contenga los siguientes campos: nombre, apellido, edad y el botón de submit.</h2>
        <p><b>Observación:</b>Se deben usar las cadenas HEREDOC.</p>
    </div>
    <div class="desarrollo">
        <h1>Formulario</h1>
        <form action="ejercicio5.php" method="post">
        <p>Inserte su nombre: <input type="text" name="nombre" id="nombre"> </p>
        <p>Inserte su apellido: <input type="text" name="apellido" id="apellido"> </p>
        <p>Inserte su edad:<input type="number" name="edad" id="edad"> </p>
        <input type="submit" value="Imprimir" onclick="imprimir()">
    </form>

  </div>
  <div class="desarrollo">
  <h1>Impresión</h1>
  <?php
            function imprimir() {
                
                $nombre = $_POST['nombre'];
                $apellido = $_POST['apellido'];
                $edad = $_POST['edad'];
                if ($nombre <> '' && $apellido <> '' && $edad > 0) {
                    $impresion = <<<TXT
                                Mi nombre es <b>$nombre $apellido</b> y tengo <b>$edad</b> años.
                            TXT;
                echo '<h4>' . $impresion . '</h4>';
                }
                else {
                    echo '<h4><b>Complete todos los campos...</b></h4>';
                }
            }
        ?>
  <?php
        imprimir();
    ?>
  </div>
</body>
</html>