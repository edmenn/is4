<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 15 - Ejercitario 3</title>
  <link href="css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Declarar un vector de 20 elementos con valores aleatorios de 1 a 10.
            Crear una función recursiva en PHP que recorra el array de atrás para adelante. Se deberá
            imprimir desde el último elemento del array hasta el primero</h2>
            <p><b>Observación:</b>El alumno deberá crear sus propias funciones para realizar este ejercicio.</p>
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <?php
		/*Cadena Heredoc, permite expandir variables en PHP*/
		$str=<<<HTML
			<form action="#" method="post">
				<p><b>Generar Array Recursivo</b></p>
				<br>
				<input type="submit" id="btnSubmit" name="btnSubmit" value="Generar Array" />
			</form>
		HTML;

		if (isset($_POST["btnSubmit"])){
			
			$array_aleatorio = generarArrayRandom(20);
			echo "<b>Listando el \$array_aleatorio : </b><br>";
			print_r($array_aleatorio);
			
			echo "<br><br>";
			echo "<b>Imprimiendo con recursividad de atrás para delante: </b><br>";
			imprimirUltimo($array_aleatorio);
			echo "<br/><br/><a href='ejercicio15.php'>Volver</a><br/>";
		} else {
			echo $str;
		}

		function generarArrayRandom($tamaño)
		{
			$resultado = Array();
			for ($i = 0; $i < $tamaño; $i++)
			{
				$resultado += [$i => rand(1, 10)];
			}
			return $resultado;
		}

		function imprimirUltimo($array_aleatorio)
		{
			echo "Valor: " . array_pop($array_aleatorio) . "<br>";
			if (count($array_aleatorio) > 0) 
			{
				imprimirUltimo($array_aleatorio);
			}
		}
	?>
  </div>
</body>
</html>