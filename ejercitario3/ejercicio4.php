<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 4 - Ejercitario 3</title>
  <link href="css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Hacer un script PHP que haga lo siguiente:</h2>
           <ul>
               <li>Declarar 3 variable con valores enteros aleatorios (se debe generar aleatoriamente estos valores entre 50 y 900) – Se debe usar la función mt_srand y mt_rand.</li>
               <li>Ordenar de mayor a menor los valores de estas variables.</li>
               <li>Imprimir en pantalla la secuencia de números ordenadas, los números deben estar separados por espacios en blanco.</li>
               <li>El número mayor debe estar en color verde y el número más pequeño debe estar en color rojo.</li>
           </ul>
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <?php
        function crear_semilla()
        {
          list($usec, $sec) = explode(' ', microtime());
          return (float) $sec + ((float) $usec * 100000);
        }
        mt_srand(crear_semilla());
        $aleatorios = array(mt_rand(50,900) , mt_rand(50,900) , mt_rand(50,900));

        rsort($aleatorios); //ordenamos el array descendentemente

        $v3 = $aleatorios[2];
        $v2 = $aleatorios[1];
        $v1 = $aleatorios[0];
        echo <<< EOL
        <div class=texto>
        EOL;
        echo '<div>' . $v1 . '</div> ';
        echo '<div>' . $v2 . '</div> ';
        echo '<div>' . $v3 . '</div>';
        echo <<< EOL
        </div>
        EOL;
    ?>
  </div>
</body>
</html>