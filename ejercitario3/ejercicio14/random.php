<?php
	function generarArrayRandom($dimension)
	{
		$resultado = array();
		for ($i = 0; $i < $dimension; $i++)
		{
			$resultado += [generarStringRandom() => $i + 1];
		}
		return $resultado;
	}
?>