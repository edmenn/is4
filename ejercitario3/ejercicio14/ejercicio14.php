<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 13 - Ejercitario 3</title>
  <link href="../css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Crear un array asociativo en PHP que cumpla los siguientes requisitos:</h2>
        <ul>
            <li>Los índices/claves deben ser strings (estas cadenas deben ser generadas aleatoriamente
                por el script y deben tener una longitud de 5 a 10 caracteres)./li>
            <li>Los valores deben ser números enteros del 1 a 1000.</li>
            <li>Se debe imprimir el vector completo indicando índice y valor.</li>
            <li>Se debe recorrer dicho array e imprimir sólo las claves del array que empiecen con la letra
                a, d, m y z (función imprimir) (en el caso se no existir ningún índice que comience con esas
l               etras se debe imprimir un mensaje).</li>
        </ul>
        <p><b>Observación:</b>Se debe crear un archivo por función y el archivo principal donde se llaman a las
                funciones y se realizan los procesamientos.</p>
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <?php
		include 'generarString.php'; // funcion generarStringRandom()
		include 'random.php'; // funcion generarArrayRandom()
		include 'buscar.php'; // funcion buscarLetrasEnArrayKeys()

		/*Cadena Heredoc, permite expandir variables en PHP*/
		$str=<<<HTML
			<form action="#" method="post">
				<p><b>Generar PHP</b></p>
				<br>
				<input type="submit" id="btnSubmit" name="btnSubmit" value="Generar PHP" />
			</form>
		HTML;

		if (isset($_POST["btnSubmit"])){
			
			$array_aleatorio = generarArrayRandom(20); 
			$array_admz = buscarLetrasEnArrayKeys($array_aleatorio, array("a", "d", "m", "z"));
			
			echo "<b>Listando el \$array_aleatorio : </b><br>";
			print_r($array_aleatorio);
			
			echo "<br/><br/>";

			if (count($array_admz) == 0) 
			{
				echo "<b>NO HAY elementos que su Key empiezan por  a, d, m y z.</b><br>";
			} else 
			{
				echo "<b>Elementos que su Key empiezan por  a, d, m y z: </b><br>";
				print_r($array_admz);
			}
			
			echo "<br/><br/><a href='ejercicio14.php'>Volver</a><br/>";
		} else {
			echo $str;
		}
	?>
  </div>
</body>
</html>