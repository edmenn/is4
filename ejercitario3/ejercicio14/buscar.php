<?php
	function buscarLetrasEnArrayKeys($array_aleatorio, $array_letras)
	{
		$resultado = array();
		foreach($array_aleatorio as $key => $value)
		{
			foreach($array_letras as $key_letras => $value_letras)
			{
				if (substr($key,0,1) == $value_letras) 
				{
					//echo "Indice: $key == Valor: $value";
					$resultado += [$key => $value];
				}
			}
		}
		return $resultado;
	}
?>