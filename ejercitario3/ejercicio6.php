<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 6 - Ejercitario 3</title>
  <link href="css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Hacer un script en PHP que simule una calculadora con las operaciones básicas: suma, resta, multiplicación y división.</h2>
        <ul>
            <li>El usuario debe introducir el operando 1.</li>
            <li>El usuario debe introducir el operando 2.</li>
            <li>El usuario debe seleccionar la operación que quiere realizar (a través de un select).</li>
            <li>El usuario debe presionar el botón calcular.</li>
            <li>El script debe calcular el resultado de la operación e imprimirlo en pantalla.</li>
        </ul>
        <p><b>Observación:</b>El script debe realizar los controles respectivos (para casos donde las operaciones no son válidas).</p>
    </div>
    <div class="desarrollo">
        <h1>Formulario</h1>
        <form action="ejercicio6.php" method="post">
        <p>
            <label for="numero_1">Operando 1</label>     
            <input type="number" name="numero_1" id="numero_1">
        </p>
        <p>
            <label for="numero_2">Operando 2:</label>
            <input type="number" name="numero_2" id="numero_2">
        </p>
        <select name="operacion" id="operacion">
            <option value="sumar">Sumar</option>
            <option value="restar">Restar</option>
            <option value="multiplicar">Multiplicar</option>
            <option value="dividir">Dividir</option>
        </select>
        <input type="submit" value="Calcular" onclick="calcular()">
    </form>
    </form>
    <?php
        calcular();
    ?>
  </div>
  <?php
            function calcular() {
                error_reporting(E_ALL ^ E_NOTICE);
                        $numero_1 = $_POST['numero_1'];
                $numero_2 = $_POST['numero_2'];
                $operacion = $_POST['operacion'];
                switch ($operacion) {
                    case 'sumar':
                        $resultado = $numero_1 + $numero_2;
                        break;
                    case 'restar':
                        $resultado = $numero_1 - $numero_2;
                        break;
                    case 'multiplicar':
                        $resultado = $numero_1 * $numero_2;
                        break;
                    case 'dividir':
                        if ($numero_2 == 0) {
                            $resultado = 'No se puede dividir un número entre 0...';
                        }
                        else {
                            $resultado = $numero_1 / $numero_2;
                        }
                        break;
                    default:
                        break;
                }
                $tipo_resultado = gettype($resultado);
                if ($tipo_resultado == 'string') {
                    echo '<h4><b>' . $resultado . '</b></h4>';
                }
                else {
                    echo '<h4>El resultado es: <b>' . $resultado . '</b></h4>';
                }
            }
        ?>
</body>
</html>