<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 13 - Ejercitario 3</title>
  <link href="css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Crear un array asociativo de 10 elementos. Los valores de los 10 elemento del array deben ser
            strings.<br>
            Crear un array asociativo de un elemento. El valor del array debe ser un string.<br>
            El script PHP debe hacer lo siguiente:</h2>
        <ul>
            <li>Imprimir en pantalla si existe la clave del array de un elemento en el array de 10
                elementos (si no existe, se imprime un mensaje).</li>
            <li>Imprimir en pantalla si existe el valor del vector de un elemento en el vector de 10
                elementos (si no existe, se imprime un mensaje).</li>
            <li>Si no existe ni la clave ni el valor (del vector de un elemento) en el vector mayor se inserta
                como un elemento más del vector.</li>
        </ul>
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <?php
		/*Cadena Heredoc, permite expandir variables en PHP*/
		$str=<<<HTML
			<form action="#" method="post">
				<p><b>Generar PHP</b></p>
				<br>
				<input type="submit" id="btnSubmit" name="btnSubmit" value="Generar PHP" />
			</form>
		HTML;

		if (isset($_POST["btnSubmit"])){
			$animales = [
				"Mono" => "Loco",
				"Perro" => "Pequeño",
				"Vaca" => "Grande",
				"Lobo" => "Audaz",
				"Toro" => "Imponente",
				"Gato" => "Ruidoso",
				"Leon" => "Poderoso",
				"Tigre" => "Salvaje",
				"Gallina" => "Clueca",
				"Cerdo" => "Gordo"
			];
			
			$animal = [
				"Lobos" => "Loco"
			];

			//Bandera
			$bandera = 0;

			//Verifica si existe la misma llave entre los arrays
			echo "<b>Verificando si existe la misma LLAVE entre arrays : </b>";
			if (array_key_exists(array_key_first($animal),$animales)) {
				echo "Si";
			} else {
				echo "No";
				$bandera += 1;
			}
			echo "<br><br>";
			//Verifica si existe el mismo valor entre los arrays
			echo "<b>Verificando si existe el mismo VALOR entre arrays : </b>";
			if (in_array($animal[array_key_first($animal)],$animales)) {
				echo "Si";
			} else {
				echo "No";
				$bandera += 1;
			}
			
			if ($bandera == 2) 
			{
				$resultado = array_merge($animales, $animal);
				echo "<br><br><b>Imprimiendo array resultante: </b><br>";
				print_r($resultado);
			}
			
			
			echo "<br/><br/><a href='ejercicio13.php'>Volver</a><br/>";
		} else {
			echo $str;
		}

	?>
  </div>
</body>
</html>