<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 1 - Ejercitario 3</title>
  <link href="css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <p>Hacer un script PHP que realice el siguiente cálculo:</p>
        <p>x = ((A * ¶) + B) / (C*D) - Se debe calcular e imprimir el valor de x</p>
           <p>Donde:</p>
           <ul>
               <li>A es la raiz cuadrada de 2</li>
               <li>¶ es el número PI</li>
               <li>B es es la raíz cúbica de 3</li>
               <li>C es la constante de Euler</li>
               <li>D es la constante e</li>
           </ul>
           <p><b>Observación:</b>Utilizar las constantes matemáticas definidas den la extensión math de PHP</p>
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
  <?php
    //include "css/estilo.css";
    $uno = sqrt(2); //variable para el numero 2
    $dos = pi(); //variable para le número 3
    $tres = pow(3, 1/3); //se utiliza para la raiz cubica
    $euler = M_EULER;
    $e = M_E;
    $x = (($uno * $dos) + $tres) / ($euler * $e);
    echo "<h4>x = (($uno x $dos) + $tres) / ($euler x $e) = $x</h4>";
    ?>
  </div>
</body>
</html>