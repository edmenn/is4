<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 2 - Ejercitario 3</title>
  <link href="css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <p>Hacer un script PHP que imprime la siguiente información:</p>
           <ul>
               <li>Versión de PHP utilizada.</li>
               <li>El id de la versión de PHP.</li>
               <li>El valor máximo soportado para enteros para esa versión.</li>
               <li>Tamaño máximo del nombre de un archivo.</li>
               <li>Versión del Sistema Operativo.</li>
               <li>Símbolo correcto de 'Fin De Línea' para la plataforma en uso.</li>
               <li>El include path por defecto.</li>
           </ul>
           <p><b>Observación:</b>Ver las constantes predefinidas del núcleo de PHP</p>
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <?php
    $version = phpversion();
    $idVersion = PHP_VERSION_ID;
    $maxEnteros = PHP_INT_MAX;
    $maxNombre = PHP_MAXPATHLEN;
    $sistemaOperativo = PHP_OS;
    $simboloFinalLinea = PHP_EOL;
    $includePath = DEFAULT_INCLUDE_PATH;
    echo '<ul>
            <li><b>Versión de PHP utilizada:</b> ' . $version . '</li>
            <li><b>El ID de la versión de PHP:</b> ' . $idVersion . '</li>
            <li><b>El valor máximo soportado para enteros para esa versión:</b> ' . $maxEnteros . '</li>
            <li><b>Tamaño máximo del nombre de un archivo:</b> ' . $maxNombre . '</li>
            <li><b>Versión del Sistema Operativo:</b> ' . $sistemaOperativo . '</li>
            <li><b>Símbolo correcto de "Fin De Línea" para la plataforma en uso:</b> ' . $simboloFinalLinea . '</li>
            <li><b>El include path por defecto:</b> ' . $includePath . '</li>
          </ul>';
  ?>
  </div>
</body>
</html>