<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 12 - Ejercitario 3</title>
  <link href="css/estilo.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <div class="contenedor">
        <h1>Enunciado</h1>
        <h2>Hacer un script PHP que haga lo siguiente:</h2>
        <ul>
            <li>Crear un array indexado con 10 cadenas.</li>
            <li>Crear una variable que tenga un string cualquiera como string.</li>
        </ul>
        <h2>El script debe:</h2>
        <ul>
            <li>Buscar si la cadena declarada está en el vector declarado.</li>
            <li>Si esta, se imprime “Ya existe” la cadena.</li>
            <li>Si no está, se imprime “Es Nuevo” y se agrega al Final del array.</li>
        </ul>
        <p><b>Observación:</b>El alumno deberá crear sus propias funciones para realizar este ejercicio.</p>
    </div>
    <div class="desarrollo">
        <h1>Desarrollo</h1>
        <?php
		/*Cadena Heredoc, permite expandir variables en PHP*/
        $cadenaArray = array("frase 1", "frase 2", "frase 3", "frase 4", "frase 5", "frase 6", "frase 7", "frase 8", "frase 9", "frase 10");
		print_r($cadenaArray);
        $cadena_x = "frase 10";
        echo "<h4>La cadena elegida es $cadena_x</h4>";
		$str=<<<HTML
			<form action="#" method="post">
				<p><b>Generar PHP</b></p>
				<br>
				<input type="submit" id="btnSubmit" name="btnSubmit" value="Generar PHP" />
			</form>
		HTML;

        if (isset($_POST["btnSubmit"])){
			
			
			// Variable a modificar para buscar dentro del array $cadenaArray
			
			
			$resultado = verificarEnVector($cadenaArray, $cadena_x);
			
			echo "<b>Buscando si la cadena añadida ya existe en el array: </b><br>";
			echo "<p>$resultado</p>";
            if ($resultado=="Es nuevo"){
                array_push($cadenaArray, $cadena_x);
            }
            print_r($cadenaArray);
			echo "<br/><br/><a href='ejercicio12.php'>Volver al Generador</a><br/>";
		} else {
			echo $str;
		}
        
		function verificarEnVector($cadenaArray, $cadena_x)
		{
			if (in_array($cadena_x, $cadenaArray))
			{   
				return "Ya existe";
			} else 
			{
            	return "Es nuevo";
			}
		}
	?>
  </div>
</body>
</html>