<?php
    define('DB_SERVER', 'localhost');
    define('DB_USERNAME', 'postgres');
    define('DB_PASSWORD', 'postgres');
    define('DB_NAME', 'ej5_ejercicio2');
    define('DB_PORT', '5432');

    $conn_string = "host=" . DB_SERVER . " port=" . DB_PORT . " dbname=" . DB_NAME . " user=" . DB_USERNAME . " password=" . DB_PASSWORD;
    $conn = pg_connect($conn_string);

    // Check connection
    if($conn === false){
    die("ERROR: No se pudo establecer la conexión con PostgreSQL.");
    }
?>