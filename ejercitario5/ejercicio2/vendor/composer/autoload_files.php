CREATE TABLE alumnos (
  id              SERIAL PRIMARY KEY,
  nombre          VARCHAR(40) NOT NULL,
  apellido        VARCHAR(40) NULL NULL,
  cedula          VARCHAR(40) NOT NULL,
  matricula       VARCHAR(40) NULL NULL,
  carrera         VARCHAR(40) NOT NULL,
  nacionalidad    VARCHAR(40) NULL NULL
);
