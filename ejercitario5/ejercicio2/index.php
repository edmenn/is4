<?php
	include_once ('conf.php');
	include('base.php');
	$consulta = pg_query($conn, "SELECT id, nombre, apellido, cedula, matricula, carrera, nacionalidad FROM alumnos ORDER BY id");

	if (!$consulta) {
		echo "Ocurrió un error al consultar";
		exit;
	} else {
				$str=<<<HTML
				<div class="container">
					
				
				<h1>LISTADO DE ALUMNOS <span><a class="btn btn-primary mb-2"href='insert.php'>Insertar Alumnos desde Planilla Excel</a></span></h1>
				<div class="table-responssive">
					
				
				<table class="table table-bordered" id="table" data-toggle="table" data-height="200">
					<thead class="thead-dark">
					
						<tr>	
						<th scope="col"> # </th>
						<th scope="col"> Nombre </th>
						<th scope="col"> Apellido </th>
						<th scope="col"> Cedula </th>
						<th scope="col"> Matricula </th>
						<th scope="col"> Carrera </th>
						<th scope="col"> Nacionalidad </th>
						<th scope="col"> ¿Borrar? </th>
						</tr>
					</thead>
				
				HTML;
				while ($row = pg_fetch_row($consulta)) {
				$str.=<<<HTML
					<tr>
						<td '> $row[0] </td>
						<td '> $row[1] </td>
						<td '> $row[2] </td>
						<td '> $row[3] </td>
						<td '> $row[4] </td>
						<td '> $row[5] </td>
						<td '> $row[6] </td>
						<td > <a class="btn btn-danger" href='borrar.php?id=$row[0]'> Borrar </a> </td>
					</tr>
					HTML;
				}
				$str.=<<<HTML
				</table>
				</div>
				
				</div>
				HTML;
				
			}
		echo $str;
?>