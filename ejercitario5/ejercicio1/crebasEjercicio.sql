/*==============================================================*/
/* Table: MARCA                                                 */
/*==============================================================*/
create table MARCA (
   MARCA_ID             SERIAL               not null,
   NOMBRE               VARCHAR(40)          null,
   constraint PK_MARCA primary key (MARCA_ID)
);

/*==============================================================*/
/* Index: MARCA_PK                                              */
/*==============================================================*/
create unique index MARCA_PK on MARCA (
MARCA_ID
);

/*==============================================================*/
/* Table: PRODUCTO                                              */
/*==============================================================*/
create table PRODUCTO (
   PRODUCTO_ID          SERIAL               not null,
   TIPO_ID              INT4                 not null,
   MARCA_ID             INT4                 not null,
   NOMBRE               VARCHAR(40)          null,
   DESCRIPCION          VARCHAR(40)          null,
   constraint PK_PRODUCTO primary key (PRODUCTO_ID)
);

/*==============================================================*/
/* Index: PRODUCTO_PK                                           */
/*==============================================================*/
create unique index PRODUCTO_PK on PRODUCTO (
PRODUCTO_ID
);

/*==============================================================*/
/* Index: PROD_MAR_FK                                           */
/*==============================================================*/
create  index PROD_MAR_FK on PRODUCTO (
MARCA_ID
);

/*==============================================================*/
/* Index: PROD_TIP_FK                                           */
/*==============================================================*/
create  index PROD_TIP_FK on PRODUCTO (
TIPO_ID
);

/*==============================================================*/
/* Table: TIPO                                                  */
/*==============================================================*/
create table TIPO (
   TIPO_ID              SERIAL               not null,
   NOMBRE               VARCHAR(40)          null,
   constraint PK_TIPO primary key (TIPO_ID)
);

/*==============================================================*/
/* Index: TIPO_PK                                               */
/*==============================================================*/
create unique index TIPO_PK on TIPO (
TIPO_ID
);

alter table PRODUCTO
   add constraint FK_PRODUCTO_PROD_MAR_MARCA foreign key (MARCA_ID)
      references MARCA (MARCA_ID)
      on delete restrict on update restrict;

alter table PRODUCTO
   add constraint FK_PRODUCTO_PROD_TIP_TIPO foreign key (TIPO_ID)
      references TIPO (TIPO_ID)
      on delete restrict on update restrict;

insert into tipo values(1,'Bebidas Nutritivas');
insert into tipo values(2,'Bebidas Alcoholicas');
insert into tipo values(3,'Gaseosas');
insert into tipo values(4,'Panificados');
insert into tipo values(5,'Verduras');
insert into tipo values(6,'Lacteos');
insert into tipo values(7,'Carnes');
ALTER SEQUENCE tipo_tipo_id_seq RESTART WITH 8;

insert into marca values(1,'Coca Cola');
insert into marca values(2,'Pechugon');
insert into marca values(3,'Ochi');
insert into marca values(4,'La Negrita');
insert into marca values(5,'Trebol');
insert into marca values(6,'Do�a Angela');
insert into marca values(7,'Miller');
insert into marca values(8,'Heineken');
insert into marca values(9,'Pepsi');
insert into marca values(10,'Cervepar');
ALTER SEQUENCE marca_marca_id_seq RESTART WITH 11;

insert into producto values(1,2,7,'Miller 3/4','Mil....');
insert into producto values(2,2,7,'Miller Latita','Mil....');
insert into producto values(3,2,8,'Heineken 1L','Hei....');
insert into producto values(4,2,10,'Brahma 3/4','Bra....');
insert into producto values(5,2,10,'Pilsen 3/4','Bra....');
insert into producto values(6,3,1,'Naranja 500ml','Fanta Naranja');
insert into producto values(7,6,6,'Flan Chico','Fla..');
insert into producto values(8,7,2,'Pechuga de Pollo','Bandeja de 400g');
insert into producto values(9,7,2,'Muzlo de Pollo','Bandeja de 400g');
ALTER SEQUENCE producto_producto_id_seq RESTART WITH 10;

/*==============================================================*/
/* Insertar Usuarios                                            */
/*==============================================================*/
-- Añadir la extension pgcrypto
CREATE EXTENSION IF NOT EXISTS pgcrypto;

-- DDL
CREATE TABLE USUARIOS (
   ID                   SERIAL               not null,
   USUARIO              VARCHAR(35)         null,
   PASSWORD             VARCHAR(35)         null,
   CONSTRAINT PK_USUARIOS PRIMARY KEY (ID)
);
-- INSERTAR USUARIO
INSERT INTO USUARIOS VALUES (1, 'usuario', crypt('usuario', gen_salt('md5')));