<?php
	include_once ('config.php');
	include('base.php');
	$consulta = pg_query($conn, "SELECT p.producto_id, p.nombre, p.descripcion, m.nombre, t.nombre FROM producto p, marca m,tipo t WHERE p.marca_id = m.marca_id AND p.tipo_id = t.tipo_id ORDER BY p.producto_id");

	if (!$consulta) {
		echo "Ocurrió un error al consultar";
		exit;
	} else {
				$str=<<<HTML
				<div class="container">
					
				
				<h1>LISTADO DE PRODUCTOS <span><a class="btn btn-primary mb-2"href='insert.php'>Insertar Producto</a></span></h1>
				<div class="table-responssive">
					
				
				<table class="table table-bordered" id="table" data-toggle="table" data-height="200">
					<thead class="thead-dark">
					
						<tr>	
						<th scope="col"> Nro. Producto </th>
						<th scope="col"> Nombre </th>
						<th scope="col"> Descripcion </th>
						<th scope="col"> Marca </th>
						<th scope="col"> Tipo </th>
						<th scope="col"> ¿Borrar? </th>
						<th scope="col"> ¿Editar? </th>
						</tr>
					</thead>
				
				HTML;
				while ($row = pg_fetch_row($consulta)) {
				$str.=<<<HTML
					<tr>
						<td '> $row[0] </td>
						<td '> $row[1] </td>
						<td '> $row[2] </td>
						<td '> $row[3] </td>
						<td '> $row[4] </td>
						<td > <a class="btn btn-danger" href='borrar.php?id=$row[0]'> Borrar </a> </td>
						<td > <a class="btn btn-warning" href='edit.php?id=$row[0]'> Editar </a> </td>
					</tr>
					HTML;
				}
				$str.=<<<HTML
				</table>
				</div>
				
				</div>
				HTML;
				
			}
		echo $str;
?>