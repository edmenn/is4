<?php
    define('DB_SERVER', 'localhost');
    define('DB_USERNAME', 'postgres');
    define('DB_PASSWORD', 'postgres');
    define('DB_NAME', 'ej5_ejercicio1');
    
    $conn_string = "host=" . DB_SERVER . " port=5432 dbname=" . DB_NAME . " user=" . DB_USERNAME . " password=" . DB_PASSWORD;
    $conn = pg_connect($conn_string);
    
    // Check connection
    if ($conn === false) {
        die("ERROR: No se pudo establecer la conexión con PostgreSQL.");
    }
?>