<?php
	include('base.php');
	// Crea la Conexion
	include_once ('config.php');

	if (!$conn) {
		echo "Ocurrió un error al conectar";
		exit;
	} else {
	
		$str=<<<HTML
		<main role="main" class="container my-auto">
			<div class="row" > 
				<div id="login" class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-12">
				<h2 class="text-center">INICIO DE SESIÓN</h2>
				<img class="img-fluid mx-auto d-block rounded"
				src="https://c.pxhere.com/images/45/bd/2e544c39792d55c63c2439d64a31-1440395.jpg!d" />

			
		
					<form action="#" method="post">
						<div class="form-group">
							<label for="usuario">Usuario:</label>
							<input class="form-control" type="text" name="usuario" placeholder="Introduzca su usuario" />
						</div>
						<div class="form-group">
							<label for="password">Password:</label>
							<input class="form-control" type="password" name="password" placeholder="Introduzca su password" />
						</div>
						<div ">
							<button class="btn btn-primary mb-2" type="submit">Iniciar Sesión</button>
						</div>
					</form>
				</div>
			</div>
		</main>
			
	HTML;

		if (!isset($_POST['usuario']) && !isset($_POST['password'])) {
			echo $str; 
		} else {
			$usuario = $_POST['usuario'];
			$password = $_POST['password'];
			
			
			$resultado= pg_query($conn, "SELECT * from usuarios WHERE usuario = '$usuario' AND password = crypt('$password', password)");

			if (!$resultado) {
				echo "Ocurrió un error al consultar";
				exit;
			} else {
				
				if (pg_affected_rows($resultado) == 1) {
					header("Location: home.php");
				} else {
					echo "Se ingresó un usuario y/o contraseña erronea.<br>";
					echo "<a href='ejercicio_1.php'> Volver a intentar </a>";
				}
			}
		}
	}
?>