<?php
$str=<<<HTML
		<main role="main" class="container my-auto">
			<div class="row" > 
				<div id="login" class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-12">
				<h2 class="text-center">INSERTAR PRODUCTO</h2>
				<form action="#" method="post">
			
			<div class="form-group">
			<label for="nombre">Nombre del Producto:</label>
			<input class="form-control" type="text" name="nombre" value="$nombre">
			</div>
			<div class="form-group"> 
			<label for="descripcion">Descripción:</label>
			<input class="form-control" type="text" name="descripcion" value="$descripcion">
		</div>
		<div class="form-group"> 
			<label for="marca">Marca:</label>
			<select class="form-control" id="marca" name="marca">
HTML;
			while ($row = pg_fetch_row($marcas)) {
				if ($marca == $row['0']) {
					$str .= "<option value='" . $row['0'] . "' selected>" . $row['1'] . "</option>";
				} else {
					$str .= "<option value='" . $row['0'] . "'>" . $row['1'] . "</option>";
				}
			}
$str.=<<<HTML
			</select>
		</div>
		<div class="form-group"> 
			<label for="tipo">Tipo:</label>
			<select class="form-control" id="tipo" name="tipo">
HTML;
			while ($row = pg_fetch_row($tipos)) {
				if ($tipo == $row['0']) {
					$str .= "<option value='" . $row['0'] . "' selected>" . $row['1'] . "</option>";
				} else {
					$str .= "<option value='" . $row['0'] . "'>" . $row['1'] . "</option>";
				}
			}
$str.=<<<HTML
			</select>
		</div>
		<br/>
		<div class="button">
			<button class="btn btn-primary" type="submit">Guardar Cambios</button>
		</div>
	</form>
	</div>
	</main>
	HTML;
    ?>