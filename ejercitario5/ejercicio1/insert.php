<?php
	// Crea la Conexion
	include_once ('config.php');
	include('base.php');
	$marcas = pg_query($conn, "SELECT * FROM marca");
	$tipos = pg_query($conn, "SELECT * FROM tipo");

	include('formulario.php');

	if (!isset($_POST['nombre']) && !isset($_POST['descripcion'])) {
		echo $str; //Imprimo el formulario cuando no me llega información por Post
	} else {
		$tipo = $_POST['tipo'];
		$marca = $_POST['marca'];
		$nombre = $_POST['nombre'];
		$descripcion = $_POST['descripcion'];
		
		// Obtener el ultimo producto_id
		$resultado_product_id = pg_query($conn, "SELECT producto_id FROM producto ORDER BY producto_id DESC LIMIT 1");
		$ultimo_product_id = pg_fetch_row($resultado_product_id);
		$ultimo_product_id = $ultimo_product_id[0] + 1;

		// Se hace el select con al crypt para verificar si la contraseña esta correcta.
		$resultado= pg_query($conn, "INSERT INTO producto VALUES ($ultimo_product_id, $tipo, $marca, '$nombre', '$descripcion')");

		if (!$resultado) {
			echo "Ocurrió un error al consultar";
			exit;
		} else {
			// Se verifica si hubo 1 registro afectado (login correcto). Mostrar registros.
			if (pg_affected_rows($resultado) == 1) {
				header("Location: home.php");
			} else {
				echo "ERROR al Insertar.<br>";
			}
		}
	}
?>