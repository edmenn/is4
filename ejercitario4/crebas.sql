--Tabla de Producto
create table Producto(
    id_producto serial primary key,
    id_marca int4,
    id_categoria int4,
    nombre varchar(100),
    precio decimal
);
--Tabla de categoria
create table Categoria(
    id_categoria serial primary key,
    nombre varchar(100)
);
--Tabla de Marca
create table Marca(
    id_marca serial primary key,
    id_empresa int4,
    nombre varchar(100)
);
--Tabla de Empresa
create table empresa(
    id_empresa serial primary key,
    nombre varchar(100)
);
ALTER TABLE Marca
ADD CONSTRAINT FK_MARCA_PERTENECE_EMPRESA FOREIGN KEY (id_empresa) REFERENCES empresa (id_empresa);

ALTER TABLE Producto
ADD CONSTRAINT FK_PRODUCTO_TIENE1_CATEGORIA FOREIGN KEY (id_categoria) REFERENCES Categoria (id_categoria),
ADD CONSTRAINT FK_PRODUCTO_TIENE_MARCA FOREIGN KEY (id_marca) REFERENCES Marca (id_marca);