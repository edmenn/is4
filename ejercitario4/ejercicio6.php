<?php
		/*Cadena Heredoc, permite expandir variables en PHP*/
		$str=<<<HTML
			<form action="#" method="post">
				<div>
					<label for="usuario">Usuario:</label>
					<input type="text" name="usuario" placeholder="Introduzca su usuario" />
				</div>
				<div>
					<label for="password">Password:</label>
					<input type="password" name="password" placeholder="Introduzca su contraseña" />
				</div>

				<br/>
				<div class="button">
					<button type="submit">Iniciar Sesión...</button>
				</div>
			</form>
		HTML;

		if (!isset($_POST['usuario']) && !isset($_POST['password'])) {
			echo $str; //Imprimo el formulario cuando no me llega información por Post
		} else {
			// Crea la Conexion
			$conn = pg_connect("host=localhost port=5432 dbname=ejercicio6 user=postgres password=postgres");

			if (!$conn) {
				echo "Ocurrió un error al conectar";
				exit;
			}

			$usuario = $_POST['usuario'];
			$password = $_POST['password'];
			
			// Se hace el select con al crypt para verificar si la contraseña esta correcta.
			$resultado = pg_query($conn, "SELECT * from usuarios WHERE usuario = '$usuario' AND password = crypt('$password', password)");

			if (!$resultado) {
				echo "Ocurrió un error al consultar";
				exit;
			}
			
			// Se verifica si hubo 1 registro afectado (login correcto).
			if (pg_affected_rows($resultado) == 1) {
				echo "Sesión iniciada correctamente.";
				echo "<br/><br/><a href='ejercicio6.php'>Volver a la pantalla de login</a>";
			} else {
				echo "Datos erróneos, volver a intentarlo.";
				echo "<br/><br/><a href='ejercicio6.php'>Volver a la pantalla de login</a>";
			}

			// Se cierra la conexion.
			pg_close($conn);
		}
	?>