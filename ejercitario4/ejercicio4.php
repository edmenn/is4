<?php
		try {
			$conn = new PDO('pgsql:host=localhost;dbname=ejercicio1', 'postgres', 'postgres');

			$query = "SELECT p.nombre, p.precio, m.nombre, e.nombre, c.nombre FROM producto p, marca m, empresa e, categoria c WHERE p.id_marca = m.id_marca AND p.id_categoria = c.id_categoria AND m.id_empresa = e.id_empresa";
			$sql = $conn->prepare($query);
			$sql->execute();
			$resultado = $sql->fetchAll();

			echo "<table>";
			echo "<tr>";
			echo "<td style='border:1px solid black; background-color:blue; color:white;'> Nom. Producto </td>";
			echo "<td style='border:1px solid black; background-color:blue;color:white;'> Precio</td>";
			echo "<td style='border:1px solid black; background-color:blue;color:white;'> Marca </td>";
			echo "<td style='border:1px solid black; background-color:blue;color:white;'> Empresa </td>";
			echo "<td style='border:1px solid black; background-color:blue;color:white;'> Categoría </td>";
			echo "</tr>";

				foreach ($resultado as $row) {
					echo "<tr>";
					echo "<td style='border:1px solid black;'> {$row['0']} </td>";
					echo "<td style='border:1px solid black;'> {$row['1']} </td>";
					echo "<td style='border:1px solid black;'> {$row['2']} </td>";
					echo "<td style='border:1px solid black;'> {$row['3']} </td>";
					echo "<td style='border:1px solid black;'> {$row['4']} </td>";
					echo "</tr>";
				}
				
			echo "</table>";

			$conn = null;
		} catch (PDOException $e) {
			echo "ERROR: " . $e->getMessage();
		}
	?>