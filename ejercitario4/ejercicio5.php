<?php
		// Crea la Conexion
		$conn = pg_connect("host=localhost port=5432 dbname=ejercicio5 user=postgres password=postgres");

		if (!$conn) {
			echo "No se pudo conectar";
			exit;
		}
		
		// Borra todos los datos que existen dentro de la tabla de la BD ejercicio5.
		$resultado= pg_query($conn, "DELETE FROM tabla");

		// Inserta los 1000 registros, con strings aleatorios. Se usa la funcion GenerarString.
		for ($i = 0; $i < 1000; $i++) {
			$dato_nombre = GenerarTexto(40);
			$dato_descripcion = GenerarTexto(250);
			$consulta="INSERT INTO tabla VALUES($i + 1, '$dato_nombre', '$dato_descripcion')";
			$resultado= pg_query($conn, $consulta);
		}

		// Se consulta los datos de la tabla, para mostrar en HTML
		$resultado = pg_query($conn, "SELECT * from tabla");

		if (!$resultado) {
			echo "Ocurrió un error al consultar";
			exit;
		}

		// Se muestran los datos tabulados.
		echo "<table>";
			echo "<tr>";
				echo "<td style='border:1px solid black; background-color:blue; color:white;'> ID </td>";
				echo "<td style='border:1px solid black; background-color:blue; color:white;'> Nombre </td>";
				echo "<td style='border:1px solid black; background-color:blue; color:white;'> Descripción </td>";
			echo "</tr>";

		while ($row = pg_fetch_row($resultado)) {
			echo "<tr>";
				echo "<td style='border:1px solid black;'> $row[0] </td>";
				echo "<td style='border:1px solid black;'> $row[1] </td>";
				echo "<td style='border:1px solid black;'> $row[2] </td>";
			echo "</tr>";
		}
		echo "</table>";

		// Se cierra la conexion.
		pg_close($conn);


		function generarTexto($largoString) { 
		//Funcion para generar strings aleatorios
			$caracteres = 'abcdefghijklmnopqrstuvwxyz';
			$largo = $largoString;
			$resultado = "";

			for ($i = 0; $i < $largo; $i++) 
			{
				$resultado .= $caracteres[rand(0, 25)];
			}
			return $resultado;
		}
	?>