CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE USUARIOS (
   ID                   SERIAL              not null,
   USUARIO              VARCHAR(35)         null,
   PASSWORD             VARCHAR(35)         null,
   CONSTRAINT PK_USUARIOS PRIMARY KEY (ID)
);

INSERT INTO USUARIOS VALUES (1, 'edmenn', crypt('edmenn', gen_salt('md5')));
INSERT INTO USUARIOS VALUES (2, 'edmenn2', crypt('edmenn2', gen_salt('md5')));