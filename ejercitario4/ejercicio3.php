	<?php
		$conn = pg_connect("host=localhost port=5432 dbname=ejercicio1 user=postgres password=postgres");

		if (!$conn) {
			echo "Ocurrió un error al conectar";
			exit;
		}
		//$consulta = pg_query($conn, "SELECT p.producto_id, p.nombre, p.descripcion, m.nombre, t.nombre FROM producto p, marca m,tipo t WHERE p.marca_id = m.marca_id AND p.tipo_id = t.tipo_id ORDER BY p.producto_id");
		$consulta = pg_query($conn, "SELECT p.nombre, p.precio, m.nombre, e.nombre, c.nombre FROM producto p, marca m, empresa e, categoria c WHERE p.id_marca = m.id_marca AND p.id_categoria = c.id_categoria AND m.id_empresa = e.id_empresa");
	if (!$consulta) {
		echo "Ocurrió un error al consultar";
		exit;
	} else {
		echo "<div><b> LISTADO DE PRODUCTOS </b></div>";
		echo "<table>"; //Tabla con Productos
		echo "<tr>";
				echo "<td style='border:1px solid black; background-color:blue; color:white;'> Nom. Producto </td>";
				echo "<td style='border:1px solid black; background-color:blue;color:white;'> Precio</td>";
				echo "<td style='border:1px solid black; background-color:blue;color:white;'> Marca </td>";
				echo "<td style='border:1px solid black; background-color:blue;color:white;'> Empresa </td>";
				echo "<td style='border:1px solid black; background-color:blue;color:white;'> Categoría </td>";
			echo "</tr>";

		while ($row = pg_fetch_row($consulta)) {
			echo "<tr>";
				echo "<td style='border:1px solid black;'> $row[0] </td>";
				echo "<td style='border:1px solid black;'> $row[1] </td>";
				echo "<td style='border:1px solid black;'> $row[2] </td>";
				echo "<td style='border:1px solid black;'> $row[3] </td>";
				echo "<td style='border:1px solid black;'> $row[4] </td>";
			echo "</tr>";
		}
		echo "</table>";
        $conn = null;
	}